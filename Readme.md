# Admin Database Update Utility

## Purpose 

The Admin Database Update Utility was designed to manage the standard custom objects that should reside in all admin databases.

The utility also manages different versions of procedures efficiently by first comparing source/dest revisions and then only applying the source version if necessary.

The utility has been expanded to include functionality that will run ad-hoc queries against all instances.  

## What it does

The configuration of the update utility contains a set of definitions of SQL objects and ad-hoc queries.  The actual SQL definition of each object resides in its own file.

Execution is basically this:

1. Read the configuration file
2. Connect to the source server 
3. Execute the Server List query
4. Iterate over each server that was returned
5. Connect using trusted authentication
6. Iterate over each script item in the configuration
7. Read in the definition file
8. Determine if the SQL Versions match
9. Determine if the groups have at least one group in common
10. Compare to see if the object should be installed (comparing versions)
11. Execute the definition file if necessary
12. Set the Version information

## Command line parameters

The only command line parameters that are currently supported are the –d (dump) switch and the –e (dump errors) switch.  These switches will read in the configuration specified in the config file and will dump the config values to the console.  The only difference between the two is that the –e switch will only dump what the AUU considers to be invalid configuration objects.  It will not continue execution after dumping to the screen.  This switch is to be used for configuration validation.

## Separate configuration files

As of version 1.2.0 of the AUU, the configuration is split into 2 logical pieces: base and script/object.  The base configuration contains info like source query, default database, etc.  There can be only one base configuration and it takes the file name of “Admin Database Update Utility.exe.config”.  The script/object configuration file(s) can be any name and contain only the definitions of a group of objects.  

### Base Configuration

The configuration has three basic sections: configSections, scripts, and appSettings.  configSections should not be modified as it defines the scripts section.  The scripts section contains references to the script/object config files.  The appSettings section defines global settings that should be applied to the definition of all script configuration files.

### The configSections section

The above paragraph said that this section shouldn’t be changed.  Why are you reading this?  Stop reading now.

The scripts section
-------------------

Each script object contains the following attributes:

| **Attribute** | **Description** | **Required (Y/N)** |
|:--------------|:----------------|:------------------:|
| filePath      | The name of the object.  Must be a two part name (schema.object).  This is required for all types except “Q”. | Y

The appSettings section
-----------------------

This section is just like a standard .NET configuration file.  It consists of key-value pairs.  The keys are listed below:

| **Attribute** | **Description** | **Required (Y/N)** |
|:--------------|:----------------|:------------------:|
| SourceSQLServer | The server to connect to to retrieve the list of server names | Y
| ServerListQuery | See the Query format section below | Y
| SourceLoginUser | User to use to login to central server – if left blank then AUU will use integrated credentials | N
| SourceLoginPassword | Password to login to central server | N
| BaseLocation | The base location for all of the script object definition files | N
| InstallTimeoutSeconds | \* Not yet implemented | N
| DefaultDBName | The Default DB Name.  Note that you must use either the database attribute of the script object or set the DefaultDBName. | N
| ExtProperty:AUU\_Database\_Last\_Updated | The name of the extended property that the AUU will use when stamping the database with the last updated date | N
| ExtProperty:AUU\_AdminUpdate\_Application\_Version | The string to use when stamping the object and the database of the AUU version | N
| ExtProperty:AUU\_Last\_Updated | The string to use when stamping the object with the last updated date | N
| ExtProperty:AUU\_Author | The string to use when stamping the object with the author name | N
| ExtProperty:AUU\_Major\_Version\_Number | The string to use when stamping the object with the major version number.  Use caution when changing this string. | N
| ExtProperty:AUU\_Minor\_Version\_Number | The string to use when stamping the object with the minor version number.  Use caution when changing this string. | N

Query format
------------

The ServerListQuery string must be a valid bit of T-SQL that returns a single record set and contains the following columns:

| **Column Name** | **Description** | **Data Type** | **Required (Y/N)** |
|:----------------|:----------------|:--------------|:------------------:|
| ServerName | The name of the destination server | VARCHAR(212) | Y
| SQLVersion | SQL Version 2000/2005/2008 | INT | Y
| UserName | User to connect to server with.  If NULL then AUU will use integrated. | NVARCHAR(32) | Y
| Password | Password to connect with.  Only used if UserName is not NULL | NVARCHAR(32) | Y
| ServerGroups | Groups for this server | VARCHAR(100) | N

### Script/Object Configuration

This configuration file contains only one section, the scripts section.

The scripts section is the main area of the configuration and will take the most modification.  It is basically a list of definitions for SQL objects

The scripts section
-------------------

Each script object contains the following attributes:

| **Attribute** | **Description** | **Required (Y/N)** |
|:--------------|:----------------|:------------------:|
| name | The name of the object.  Must be a two part name (schema.object).  This is required for all types except “Q”. | N
| MajorVersion | The major version number (the 1 in 1.2) | Y
| MinorVersion | The minor version number (the 2 in 1.2) | Y
| database | The database that this object should be installed in.  Will override the DefaultDBName config.  | N
| author | The author of the object | Y 
| location | The location of the script file that holds the definition of the object.  This can be an absolute path (C:\\whatever.sql) or it can be a relative path based on the value of the BaseLocation configuration in the appSettings section.  This value must be unique in the config file.  | Y
| groups | A comma separated list of group names (case insensitive).  Group names are only limiting, therefore if an object is part of no groups it is installed on all servers.  If groups are defined for both the server and the object then one group name must match for the object to be installed on the server.  | N
| type | The type of object to be installed (P = Procedure, V = View, T = Table, F = Function, Q = SQL Query) | Y
| buildMin | The minimum build number that this object is to be applied to.  Will only exclude installation.  Format is "x.x.x.x" (9.0.4035.0). Include as much as necessary, for example “9.0” is valid and considered equivalent to “9”, “9.0.0”, and “9.0.0.0”.  | N
| buildMax | The maximum build number that this object is to be applied to.  Will only exclude installation.  Format is the same a buildMin.  | N

Notes on group matching
-----------------------

Both the object definition and the query definition can optionally contain a groups list.  Groups are only limiting, meaning that if a particular object has no groups defined it will be installed on every SQL Server Instance defined by the query, regardless of the group list from the server list query.  The same logic is applied to the server list query, meaning that if a particular server has no groups defined it will receive all of the objects.

Both the object definition and the server list query must be a case-insensitive comma-separated list.  Whitespace counts, so don’t add it.

Here’s a little chart of the group logic:

|  | **Object has group “A”** | **Object has group “B”** | **Object has no groups** | 
|:-------------------------|-------------------------:|------------------------:|--:|
| **Server has group “A”** | Install | Do Not Install | Install 
| **Server has group “B”** | Do Not Install | Install | Install
| **Server has no groups** | Install | Install | Install

Example Base Configuration
--------------------------

	<?xml version="1.0" encoding="utf-8" ?>
	<configuration>
		<configSections>
			<section name="scripts"
				type="SQLSlayer.DBA.AdminUpdateUtility.ScriptSection, Admin Database Update Utility"/>
		</configSections>
		<scripts>
			<script filePath =”C:\\Script.xml” />
		</scripts>
		<appSettings>
			<add key="SourceSQLServer" value="sisbss1"/>
			<add key="ServerListQuery" value="SELECT ‘ServerName’,2000, ‘GoodServer’"/>
			<add key="SourceLoginUser" value=""/>
			<add key="SourceLoginPassword" value=""/>
			<add key="BaseLocation" value="E:\Production\AdminDB\Objects\"/>
			<add key="InstallTimeoutSeconds" value="60"/>
			<add key="DefaultDBName" value="admin"/>
		</appSettings>
	</configuration>

Example Script Configuration
----------------------------

	<scripts>
		<script
			name="dbo.fn_SQLServerInstallDir"
			MajorVersion="1"
			MinorVersion="0"
			author="Internet"
			location="dbo.fn_SQLServerInstallDir.sql"
			groups="GoodServer,BadServer"
			buildMin="9.0.0.0"
			buildMax="10.0.0.0"
			type="F"/>
	</scripts>
