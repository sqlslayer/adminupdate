using System;
using System.Collections;
using System.Text;
using System.Configuration;
using System.Xml.Serialization;
using System.IO;
using SQLSlayer.DBA.AdminUpdateUtility;

namespace SQLSlayer.DBA.AdminUpdateUtility
{
    /// <summary>
    /// The object definition for each object
    /// </summary>
    public class ObjectConfiguration
    {
        public ObjectConfiguration(BaseConfiguration bc)
        {
            _BaseConfiguration = bc;
        }

        #region Public Types
        public string ObjectName;
        public string Author;
        public int MajorVersion;
        public int MinorVersion;
        public string ObjectType;
        public string BuildMin;
        public string BuildMax;

        public string RelativePath = "";
        public string FullBasePath;
        
        #endregion Public Types

        #region Private Types
        private string _DatabaseName;
        private string _FileLocation;
        private BaseConfiguration _BaseConfiguration;
        private string _ObjectDefinition;
        private ArrayList _Groups = new ArrayList();
        #endregion Private Types

        #region Properties

        /// <summary>
        /// Will show if the object is OK.  
        /// </summary>
        public bool IsOK
        {
            get
            {
                if ((string.IsNullOrEmpty(ObjectName) && (ObjectType != "Q"))
                    || string.IsNullOrEmpty(Author)
                    || string.IsNullOrEmpty(ObjectType)
                    || string.IsNullOrEmpty(this.DatabaseName)
                    || string.IsNullOrEmpty(this.FileLocation)
                    || string.IsNullOrEmpty(this.ObjectDefinition)
                    )
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Will return the location of the definition file
        /// </summary>
        public string FileLocation
        {
            get
            {
                return GetFileLocation();
            }
            set
            {
                _FileLocation = value;
            }
        }

        /// <summary>
        /// Will return the name of the database for this object to be installed in
        /// </summary>
        public string DatabaseName
        {
            get
            {
                if (String.IsNullOrEmpty(_DatabaseName))
                {
                    return _BaseConfiguration.DefaultDBName;
                }
                else
                {
                    return _DatabaseName;
                }
            }
            set
            {
                _DatabaseName = value;
            }
        }

        /// <summary>
        /// Will return the definition of the object
        /// </summary>
        public string ObjectDefinition
        {
            get
            {
                if (String.IsNullOrEmpty(_ObjectDefinition))
                {
                    // Cache it
                    _ObjectDefinition = this.GetObjectDefinition();
                }
                return _ObjectDefinition;
            }

        }

        /// <summary>
        /// Returns an array of groups specific to this server
        /// </summary>
        public string[] Groups
        {
            get
            {
                string[] s = new string[_Groups.Count];

                for (int i = 0; i < _Groups.Count; i++)
                {
                    s[i] = _Groups[i].ToString();
                }
                return s;
            }
        }

        /// <summary>
        /// Returns the names of the object extended properties 
        /// </summary>
        public BaseConfiguration.ExtProperty ObjectExtPropertyName
        {
            get
            {
                return _BaseConfiguration.ObjectExtProperty;
            }
        }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Adds a comma separated list of group values to this server
        /// </summary>
        /// <param name="GroupList"></param>
        public void AddGroups(string GroupList)
        {
            if (!String.IsNullOrEmpty(GroupList))
            {
                foreach (string grp in GroupList.Split(','))
                {
                    if (grp == "*")
                    {
                        _Groups.Clear();
                        break;
                    }
                    else
                    {
                        _Groups.Add(grp);
                    }
                }
            }
        }

        #endregion Public Methods

        #region Helpers

        private string GetFileLocation()
        {
            /* Precedence of file locations
             * 1. Check the object for full file location in definition
             * 2. Check the object definition + object FullBasePath from object
             * 3. Check the object definition + object RelativePath from object
             * 4. Check the object definition + the base path
             */
           
            // Explicitly set object definition
            if (System.IO.File.Exists(_FileLocation))
            {
                return _FileLocation;
            }
            // FullBasePath + object location
            else if (System.IO.File.Exists(this.FullBasePath + _FileLocation))
            {
                return this.FullBasePath + _FileLocation;
            }
            // Base Path + RelativePath + object location
            else if (System.IO.File.Exists(System.IO.Path.GetFullPath(System.IO.Path.Combine(_BaseConfiguration.DefaultFileLocation, this.RelativePath)).ToString() + _FileLocation))
            {
                return System.IO.Path.GetFullPath(System.IO.Path.Combine(_BaseConfiguration.DefaultFileLocation, this.RelativePath)).ToString() + _FileLocation;
            }
            // Base Path + object location
            else if (System.IO.File.Exists(_BaseConfiguration.DefaultFileLocation + _FileLocation))
            {
                return _BaseConfiguration.DefaultFileLocation + _FileLocation;
            }
            // Didn't find anything, return empty
            else
            {
                return String.Empty;
            }
        }

        private string GetObjectDefinition()
        {
            string def = "";

            try
            {
                def = System.IO.File.OpenText(this.FileLocation).ReadToEnd();
            }
            catch
            {
                def = "";
            }

            return def;
        }

        #endregion Helpers

        public override string ToString()
        {
            string val;
            string nl = System.Environment.NewLine;

            val = " Script object definition:" + nl;
            val += "    Name:           " + ObjectName + nl;
            val += "    Database:       " + this.DatabaseName + nl;
            val += "    Author:         " + Author + nl;
            val += "    MajorVersion:   " + MajorVersion + nl;
            val += "    MinorVersion:   " + MinorVersion + nl;
            val += "    ObjectType:     " + ObjectType + nl;
            val += "    Location:       " + this.FileLocation + nl;
            val += "    Build Max:      " + this.BuildMax + nl;
            val += "    Build Min:      " + this.BuildMin + nl;
            val += "    Groups:         " + String.Join(",", this.Groups) + nl;
            val += "    Configuration seems valid: " + this.IsOK.ToString() + nl;

            return val;
        }

    }

    /// <summary>
    /// The base set of configuration values
    /// </summary>
    public class BaseConfiguration
    {
        public string DefaultFileLocation;
        public string InstallTimeoutSeconds;
        public string SourceServerListQuery;
        public string SourceSQLServer;
        public string SourceLoginUser;
        public string SourceLoginPassword;
        public string DefaultDBName;

        public ExtProperty ObjectExtProperty = new ExtProperty();
        public class ExtProperty
        {
            public string AUU_Database_Last_Updated = "AUU_Database_Last_Updated";
            public string AUU_AdminUpdate_Application_Version = "AUU_AdminUpdate_Application_Version";
            public string AUU_Last_Updated = "AUU_Last_Updated";
            public string AUU_Author = "AUU_Author";
            public string AUU_Major_Version_Number = "AUU_Major_Version_Number";
            public string AUU_Minor_Version_Number = "AUU_Minor_Version_Number";
        }

        public bool IsOK
        {
            get
            {
                if (string.IsNullOrEmpty(SourceServerListQuery)
                    || string.IsNullOrEmpty(SourceSQLServer))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public override string ToString()
        {
            string nl = System.Environment.NewLine;
            string val = "Base configuration:" + nl;
            val += "Default File Location:  " + DefaultFileLocation + nl;
            val += "Install Timeout:        " + InstallTimeoutSeconds + nl;
            val += "Source Server Name:     " + SourceSQLServer + nl;
            val += "Source Login:           " + SourceLoginUser + nl;
            val += "Source Password:        " + SourceLoginPassword + nl;
            val += "Default DB Name:        " + DefaultDBName + nl;
            val += "Source Server List Query: '" + SourceServerListQuery + "'" + nl;

            return val;
        }
    }

    public static class ConfigurationReader
    {
        public static BaseConfiguration GetBaseConfiguration(string ConfigurationFileLocation)
        {
            BaseConfiguration bc = new BaseConfiguration();
            Configuration config;

            // Read in the base configuration
            if (ConfigurationFileLocation == "")
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            else
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationFileLocation);
            }

            foreach (KeyValueConfigurationElement ele in config.AppSettings.Settings)
            {
                switch (ele.Key)
                {
                    case "SourceSQLServer":
                        bc.SourceSQLServer = ele.Value;
                        break;
                    case "ServerListQuery":
                        bc.SourceServerListQuery = ele.Value;
                        break;
                    case "SourceLoginUser":
                        bc.SourceLoginUser = ele.Value;
                        break;
                    case "SourceLoginPassword":
                        bc.SourceLoginPassword = ele.Value;
                        break;
                    case "BaseLocation":
                        bc.DefaultFileLocation = ele.Value;
                        break;
                    case "InstallTimeoutSeconds":
                        bc.InstallTimeoutSeconds = ele.Value;
                        break;
                    case "DefaultDBName":
                        bc.DefaultDBName = ele.Value;
                        break;
                    case "ExtProperty:AUU_Database_Last_Updated":
                        bc.ObjectExtProperty.AUU_Database_Last_Updated = ele.Value;
                        break;
                    case "ExtProperty:AUU_AdminUpdate_Application_Version":
                        bc.ObjectExtProperty.AUU_AdminUpdate_Application_Version = ele.Value;
                        break;
                    case "ExtProperty:AUU_Last_Updated":
                        bc.ObjectExtProperty.AUU_Last_Updated = ele.Value;
                        break;
                    case "ExtProperty:AUU_Author":
                        bc.ObjectExtProperty.AUU_Author = ele.Value;
                        break;
                    case "ExtProperty:AUU_Major_Version_Number":
                        bc.ObjectExtProperty.AUU_Major_Version_Number = ele.Value;
                        break;
                    case "ExtProperty:AUU_Minor_Version_Number":
                        bc.ObjectExtProperty.AUU_Minor_Version_Number = ele.Value;
                        break;
                }
            }

            if (bc.IsOK == false)
            {
                throw new Exception("Invalid base configuration");
            }

            return bc;
        }

        public static ObjectConfiguration[] ReadConfiguration(BaseConfiguration bc, string ConfigurationFileLocation)
        {
            ObjectConfiguration[] ocArray;
            ArrayList ocArrayList = new ArrayList();
            ObjectConfiguration oc;
            Configuration config;

            if (ConfigurationFileLocation == "")
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            else
            {
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationFileLocation);
            }

            if (bc.IsOK == false)
            {
                throw new Exception("Invalid base configuration");
            }

            // Get the object configuartion
            ScriptSection sysSection = config.GetSection("scripts") as ScriptSection;
            ScriptCollection oneCollection = sysSection.Scripts;

            foreach (ScriptElement oneElement in oneCollection)
            {
                string fileName = oneElement.FilePath;
                XmlSerializer xSer = new XmlSerializer(typeof(ScriptItemCollection));
                using (FileStream fStream = File.OpenRead(fileName))
                {
                    ScriptItemCollection scripts = xSer.Deserialize(fStream) as ScriptItemCollection;

                    foreach (ScriptItem item in scripts.Scripts)
                    {
                        oc = new ObjectConfiguration(bc);

                        oc.DatabaseName = item.Database;
                        oc.FileLocation = item.Location;
                        oc.MajorVersion = int.Parse(item.MajorVersion);
                        oc.MinorVersion = int.Parse(item.MinorVersion);
                        oc.ObjectName = item.Name;
                        oc.Author = item.Author;
                        oc.ObjectType = item.Type;
                        oc.BuildMax = item.BuildMax;
                        oc.BuildMin = item.BuildMin;
                        oc.AddGroups(item.Groups);

                        oc.RelativePath = scripts.RelativeBasePath ?? "";
                        oc.FullBasePath = scripts.FullBasePath ?? "";

                        // Add the new object to the array
                        ocArrayList.Add(oc);
                    }

                }
            }

            // Turn the arraylist into an array and return
            ocArray = new ObjectConfiguration[ocArrayList.Count];
            for (int i = 0; i < ocArrayList.Count; i++)
            {
                ocArray[i] = (ObjectConfiguration)ocArrayList[i];
            }

            return ocArray;
        }
    }
    
}
