using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace SQLSlayer.DBA.AdminUpdateUtility
{
    [Serializable]
    [XmlRoot(ElementName = "scripts")]
    public class ScriptItemCollection
    {
        [XmlAttribute("fullbasepath")]
        public string FullBasePath;
        [XmlAttribute("relativepath")]
        public string RelativeBasePath;
        [XmlElement(ElementName = "script")]
        public ScriptItem[] Scripts;
    }
}
