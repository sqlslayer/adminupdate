using System;
using System.Collections;
using System.Text;

namespace SQLSlayer.DBA.AdminUpdateUtility
{
    class Program
    {
        class CmdLineCfg
        {
            public string ConfigFileLocation = "";
            public bool DumpConfig = false;
            public bool DumpErrors = false;

            public VerbosityLevels VerbosityLevel = VerbosityLevels.RegularStrength;

            public enum VerbosityLevels
            {
                Error = 0,
                RegularStrength = 1,
                Noisy = 2,
                StupidNoisy = 3,
            }

            public int intVerbosityLevel
            {
                get
                {
                    return (int)VerbosityLevel;
                }
            }

        }

        static int Main(string[] args)
        {
            StandardObjects.ServerEnvironment[] ServerList;
            int ErrorCount = 0;
            BaseConfiguration BaseCfg;
            ObjectConfiguration[] objCfg;
            bool DidInstall;
            string DisplayName;
            ArrayList DBsToSkip = new ArrayList();

            // Process the command line arguments
            CmdLineCfg cfg = GetCmdLineCfg(args);

            StandardFunctions.LogWriter("Verbosity Level: " + cfg.VerbosityLevel.ToString(), (int)CmdLineCfg.VerbosityLevels.Noisy, cfg.intVerbosityLevel);

            // Get the configuration information
            StandardFunctions.LogWriter("Reading the configuration", (int)CmdLineCfg.VerbosityLevels.StupidNoisy, cfg.intVerbosityLevel);
            BaseCfg = ConfigurationReader.GetBaseConfiguration(cfg.ConfigFileLocation);
            objCfg = SQLSlayer.DBA.AdminUpdateUtility.ConfigurationReader.ReadConfiguration(BaseCfg, cfg.ConfigFileLocation);

            // If we're just supposed to dump the config then do that here and then quit
            if (cfg.DumpConfig)
            {
                // Print some basic junk
                System.Console.WriteLine("** Admin Update Utility **");
                System.Console.WriteLine(" Matt Stanford - SQLSlayer.com");
                System.Console.WriteLine(" 2010");
                System.Console.WriteLine("Version:      " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
                System.Console.WriteLine(); 
                System.Console.WriteLine(" Dumping entire configuration");
                System.Console.WriteLine();

                // Print the base config first
                System.Console.WriteLine(BaseCfg);

                // Now write out all of the object configurations
                foreach (ObjectConfiguration c in objCfg)
                {
                    System.Console.WriteLine(c);
                }

                return 0;

            }
            else if (cfg.DumpErrors)
            {
                // Print some basic junk
                System.Console.WriteLine("** Admin Update Utility **");
                System.Console.WriteLine(" Matt Stanford - SQLSlayer.com");
                System.Console.WriteLine(" 2010");
                System.Console.WriteLine("Version:      " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
                System.Console.WriteLine();
                System.Console.WriteLine(" Dumping errors only");
                System.Console.WriteLine();

                // Print the base config first
                System.Console.WriteLine(BaseCfg);

                // Now write out all of the object configurations
                foreach (ObjectConfiguration c in objCfg)
                {
                    // Only write the errors
                    if (c.IsOK == false)
                    {
                        System.Console.WriteLine(c);
                    }
                }

                return 0;
            }

            // Get the connections to SQL
            System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
            System.Data.SqlClient.SqlConnection SourceConn = new System.Data.SqlClient.SqlConnection();
            System.Data.SqlClient.SqlConnection DestConn = new System.Data.SqlClient.SqlConnection();

            builder.DataSource = BaseCfg.SourceSQLServer;

            builder.ApplicationName = "AdminUpdateUtility";
            builder.InitialCatalog = "master";
            
            // Set the password/user if there was one passed
            if (String.IsNullOrEmpty(BaseCfg.SourceLoginUser) == true)
            {
                builder.IntegratedSecurity = true;
            }
            else
            {
                builder.IntegratedSecurity = false;
                builder.UserID = BaseCfg.SourceLoginUser;
                builder.Password = BaseCfg.SourceLoginPassword;
            }

            // Connect to the source DB
            SourceConn.ConnectionString = builder.ConnectionString;

            try
            {
                StandardFunctions.LogWriter("Connecting to the config server: " + builder.DataSource, (int)CmdLineCfg.VerbosityLevels.StupidNoisy, cfg.intVerbosityLevel);
                SourceConn.Open();

                // Get the server list from the source connection
                ServerList = StandardFunctions.GetServerEnvironmentFromSQL(SourceConn, BaseCfg.SourceServerListQuery);
                SourceConn.Close();
            }
            catch (Exception ex)
            {
                StandardFunctions.LogWriter("Error while getting server list", (int)CmdLineCfg.VerbosityLevels.Error, cfg.intVerbosityLevel);
                StandardFunctions.LogWriter(ex.Message.ToString(), (int)CmdLineCfg.VerbosityLevels.Error, cfg.intVerbosityLevel);
                return 1;
            }
            finally
            {
                SourceConn.Dispose();
            }

            foreach (StandardObjects.ServerEnvironment ServerEnv in ServerList)
            {
                StandardFunctions.LogWriter("Updating " + ServerEnv.ServerName, (int)CmdLineCfg.VerbosityLevels.RegularStrength, cfg.intVerbosityLevel);
                
                builder.DataSource = ServerEnv.ServerName;

                if (ServerEnv.UseTrusted == true)
                {
                    builder.IntegratedSecurity = true;
                    builder.UserID = "";
                    builder.Password = "";
                }
                else
                {
                    // User/Pass specified for this server
                    builder.IntegratedSecurity = false;
                    builder.UserID = ServerEnv.User;
                    builder.Password = ServerEnv.Pass;
                }
                
                DestConn = new System.Data.SqlClient.SqlConnection();

                DestConn.ConnectionString = builder.ConnectionString;

                try
                {
                    DestConn.Open();

                    if (DestConn.State != System.Data.ConnectionState.Open)
                    {
                        throw new Exception("Can't connect to server");
                    }

                    // Clear out the DB Blacklist
                    DBsToSkip.Clear();

                    // Install each item from the configuration
                    foreach (ObjectConfiguration vObjCfg in objCfg)
                    {
                        DisplayName = vObjCfg.FileLocation;

                        if (String.IsNullOrEmpty(vObjCfg.ObjectName) == false)
                        {
                            DisplayName = vObjCfg.ObjectName;
                        }

                        if ((vObjCfg.IsOK) 
                            && DBsToSkip.Contains(vObjCfg.DatabaseName) == false)
                        {
                            // Looks good so far, now check the groups
                            if (ServerEnv.IsPartOfGroup(vObjCfg.Groups))
                            {

                                AdminSQLObject InstallObject = new AdminSQLObject();

                                InstallObject.Database = vObjCfg.DatabaseName;
                                InstallObject.Author = vObjCfg.Author;
                                InstallObject.MajorVersion = vObjCfg.MajorVersion;
                                InstallObject.MinorVersion = vObjCfg.MinorVersion;
                                InstallObject.BuildMax = vObjCfg.BuildMax;
                                InstallObject.BuildMin = vObjCfg.BuildMin;
                                InstallObject.Type = StandardFunctions.ConvertLetterToType(vObjCfg.ObjectType);
                                InstallObject.Name = vObjCfg.ObjectName;
                                InstallObject.CreateStatement = vObjCfg.ObjectDefinition;
                                InstallObject.SQLConnection = DestConn;

                                InstallObject.AUU_AppVersion = vObjCfg.ObjectExtPropertyName.AUU_AdminUpdate_Application_Version;
                                InstallObject.AUU_Author = vObjCfg.ObjectExtPropertyName.AUU_Author;
                                InstallObject.AUU_DBTime = vObjCfg.ObjectExtPropertyName.AUU_Database_Last_Updated;
                                InstallObject.AUU_MajVer = vObjCfg.ObjectExtPropertyName.AUU_Major_Version_Number;
                                InstallObject.AUU_MinVer = vObjCfg.ObjectExtPropertyName.AUU_Minor_Version_Number;
                                InstallObject.AUU_ObjTime = vObjCfg.ObjectExtPropertyName.AUU_Last_Updated;

                                try
                                {
                                    DidInstall = InstallObject.Install();

                                    if (DidInstall == false)
                                    {
                                        // Add an item to the blacklist
                                        if (InstallObject.DatabaseExists == false)
                                        {
                                            DBsToSkip.Add(vObjCfg.DatabaseName);
                                        }

                                        // Print out the output
                                        System.Console.WriteLine();
                                        System.Console.WriteLine("Object failed to install (" + DisplayName + ") showing the SQL output");
                                        System.Console.WriteLine();
                                        foreach (string sqlMsg in InstallObject.SQLOutput)
                                        {
                                            System.Console.WriteLine(sqlMsg);
                                        }
                                        ErrorCount++;
                                    }
                                }
                                catch (Exception iex)
                                {
                                    System.Console.WriteLine();
                                    System.Console.WriteLine("** Failed to install the '" + DisplayName + "' object on " + ServerEnv.ServerName);
                                    System.Console.WriteLine(iex.Message.ToString());
                                    System.Console.WriteLine();
                                    ErrorCount++;
                                }
                            }
                        }
                        else if (vObjCfg.IsOK == false)
                        {
                            System.Console.WriteLine("** Failed to install the '" + DisplayName + "' object on " + ServerEnv.ServerName + " because of invalid configuration");
                            ErrorCount++;
                        }

                    }

                    DestConn.Close();
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine();
                    System.Console.WriteLine("**** Failed to install SQL Objects on " + ServerEnv.ServerName);
                    System.Console.WriteLine(ex.Message.ToString());
                    System.Console.WriteLine();
                    ErrorCount++;
                }
                finally
                {
                    DestConn.Dispose();
                }
            }

            return ErrorCount;
        }

        static CmdLineCfg GetCmdLineCfg(string[] args)
        {

            CmdLineCfg cfg = new CmdLineCfg();
            int skipwhat = 1;

            for (int i = 0; i < args.Length; i += skipwhat)
            {
                switch (args[i])
                {
                    case @"-c":
                        cfg.ConfigFileLocation = args[i + 1];
                        skipwhat = 2;
                        break;
                    case @"-d":
                        cfg.DumpConfig = true;
                        skipwhat = 1;
                        break;
                    case @"-e":
                        cfg.DumpErrors = true;
                        skipwhat = 1;
                        break;
                    case @"-v":
                        cfg.VerbosityLevel = CmdLineCfg.VerbosityLevels.Noisy;
                        skipwhat = 1;
                        break;
                    case @"-vv":
                        cfg.VerbosityLevel = CmdLineCfg.VerbosityLevels.StupidNoisy;
                        skipwhat = 1;
                        break;
                    default:
                        skipwhat = 1;
                        break;
                }
            }

            return cfg;
        }
    }
}
