using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration; // remember to add reference in project
using System.Reflection;
using System.Text;

namespace SQLSlayer.DBA.AdminUpdateUtility
{
    public sealed class ScriptSection : ConfigurationSection
    {
        public ScriptSection()
        {
        }

        [ConfigurationProperty("", IsDefaultCollection = true)]
        public ScriptCollection Scripts
        {
            get
            {
                return (ScriptCollection)base[""];
            }
        }
    }
    public sealed class ScriptCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ScriptElement();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ScriptElement)element).FilePath;
        }
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }
        protected override string ElementName
        {
            get
            {
                return "script";
            }
        }
    }
    public sealed class ScriptElement : ConfigurationElement
    {
        [ConfigurationProperty("filePath", IsRequired = false, IsKey=true)]
        public string FilePath
        {
            get
            {
                return (string)base["filePath"];
            }
            set
            {
                base["filePath"] = value;
            }
        }
    }

}
