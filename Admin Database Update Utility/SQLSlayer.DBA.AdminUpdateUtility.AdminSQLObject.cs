using System;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace SQLSlayer.DBA.AdminUpdateUtility
{

    public class AdminSQLObject : IDisposable
    {
        private string _ObjectDefinition;
        private string _ObjectName;
        private string _ObjectSchema;
        private int _MajorVersion;
        private int _MinorVersion;
        private string _Author;
        private string _Database;
        private SqlConnection _conn;
        private bool _IsReadyToBeInstalled = false;
        private ObjectType _type;
        private string _BuildMin;
        private string _BuildMax;
        private bool _DBExists = false;

        // Names of extended properties
        public string AUU_DBTime;
        public string AUU_AppVersion;

        public string AUU_ObjTime;
        public string AUU_Author;
        public string AUU_MajVer;
        public string AUU_MinVer;

        private string _err;
        private ArrayList _SQLOutput;

        /// <summary>
        /// Object to determine whether or not the installed version matches the 
        /// version that we're attempting to install
        /// </summary>
        private class Version
        {
            public bool Exists = false;
            public int MajorVersion;
            public int MinorVersion;

            /// <summary>
            /// Determines if the versions match.  True if they do, false if they don't.
            /// </summary>
            /// <param name="Major">Major Version</param>
            /// <param name="Minor">Minor Version</param>
            /// <returns></returns>
            public bool Match(int Major, int Minor)
            {
                if ((Major == MajorVersion) &&
                    (Minor == MinorVersion))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public override bool  Equals(object obj)
            {
                try
                {
 	                Version o = (Version) obj;
                    if ((o.Exists == this.Exists) &&
                        (o.MajorVersion == this.MajorVersion) &&
                        (o.MinorVersion == this.MinorVersion))
                    {
                        return true;
                    }
                }
                catch
                {
                    return false;
                }

                return false;

            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

        }

        public enum ObjectType
        {
            NotSet,
            Procedure,
            View,
            Table,
            Function,
            NonObject,
        }

        public int InstallTimeout = 60;

        #region Properties
        /// <summary>
        /// The actual create statement for this object
        /// </summary>
        public string CreateStatement
        {
            get
            {
                return _ObjectDefinition;
            }
            set
            {
                _ObjectDefinition = value;
                RunReadyTest();
            }
        }

        /// <summary>
        /// Returns false if the database has not been identified as "found"
        /// </summary>
        public bool DatabaseExists
        {
            get
            {
                return _DBExists;
            }
        }

        /// <summary>
        /// The Minimum Build version that this script should be applied to
        /// </summary>
        public string BuildMin
        {
            get
            {
                return _BuildMin;
            }
            set
            {
                _BuildMin = value;
            }
        }

        /// <summary>
        /// The Maximum Build version that this script should be applied to
        /// </summary>
        public string BuildMax
        {
            get
            {
                return _BuildMax;
            }
            set
            {
                _BuildMax = value;
            }
        }
        
        /// <summary>
        /// The name of the object (2 part, please)
        /// </summary>
        public string Name
        {
            get
            {
                if (_type == ObjectType.NonObject)
                {
                    return "";
                }
                else
                {
                    return "[" + _ObjectSchema + "].[" + _ObjectName + "]";
                }
            }
            set
            {
                string tn = value;

                if (_type != ObjectType.NonObject)
                {
                    if (!string.IsNullOrEmpty(tn))
                    {
                        tn = tn.Replace("[", "").Replace("]", "");

                        if (tn.Contains("."))
                        {
                            _ObjectSchema = tn.Substring(0, tn.IndexOf('.'));
                            _ObjectName = tn.Substring(tn.IndexOf('.') + 1, tn.Length - tn.IndexOf('.') - 1);
                        }
                        else
                        {
                            throw new Exception("Please two part your object names");
                        }
                    }
                }

                //_ObjectName = value;
                RunReadyTest();
            }
        }

        /// <summary>
        /// The database to create the object in
        /// </summary>
        public string Database
        {
            get
            {
                return _Database;
            }
            set
            {
                _Database = value;
                RunReadyTest();
            }
        }

        /// <summary>
        /// Who wrote it
        /// </summary>
        public string Author
        {
            get
            {
                return _Author;
            }
            set
            {
                _Author = value;
            }
        }

        /// <summary>
        /// The Major version number (the 1 in 1.0)
        /// </summary>
        public int MajorVersion
        {
            get
            {
                return _MajorVersion;
            }
            set
            {
                _MajorVersion = value;
            }
        }

        /// <summary>
        /// The Minor version number (the 01 in 2.01)
        /// </summary>
        public int MinorVersion
        {
            get
            {
                return _MinorVersion;
            }
            set
            {
                _MinorVersion = value;
            }
        }

        /// <summary>
        /// Please only provide an open SQL connection here.  Anything other than that will break stuff.
        /// Don't be that guy.
        /// </summary>
        public SqlConnection SQLConnection
        {
            set
            {
                _conn = null;
                _conn = value;
                _conn.InfoMessage += new SqlInfoMessageEventHandler(_conn_InfoMessage);
                RunReadyTest();
            }
        }

        /// <summary>
        /// Returns each return from SQL as a new line
        /// </summary>
        public object[] SQLOutput
        {
            get
            {
                try
                {
                    return _SQLOutput.ToArray();
                }
                catch
                {
                    return new object[0];
                }
            }
        }

        /// <summary>
        /// Gets and Sets the type of object that should be installed
        /// </summary>
        public ObjectType Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
                RunReadyTest();
            }
        }

        #endregion

        /// <summary>
        /// Built to do all of the dirty work for you.
        /// Installs SQL object into databases.
        /// </summary>
        public AdminSQLObject()
        {
            this.ResetClass();
        }

        /// <summary>
        /// Installs this bitch
        /// ** Don't try to run this until you're ready **
        /// </summary>
        /// <returns>
        /// True if the install worked, false if it failed.
        /// </returns>
        public bool Install()
        {
            Server srv;
            ServerConnection conn;

            bool Worked = false;

            if (!_IsReadyToBeInstalled)
            {
                throw new Exception(_err);
            }

            if (_conn.State != System.Data.ConnectionState.Open)
            {
                throw new Exception("SQL Connection doesn't seem to be ready");
            }

            // Test
            if (ObjectShouldBeInstalled() && StandardFunctions.IsBuildWithinRange(this.BuildMin,this.BuildMax,_conn.ServerVersion))
            {
                conn = new ServerConnection(_conn);
                conn.StatementTimeout = InstallTimeout;
                srv = new Server(conn);

                // Try to find the database on this instance
                foreach (Database d in srv.Databases)
                {
                    if (d.Name == _Database)
                    {
                        _DBExists = true;
                    }
                }

                // Exit this routine
                if (_DBExists == false)
                {
                    return false;
                }

                // Give it the good old try.  See if the user's script will run.
                try
                {
                    srv.ConnectionContext.ExecuteNonQuery("USE [" + _Database + "]");

                    srv.ConnectionContext.ExecuteNonQuery(_ObjectDefinition);
                    if (_type == ObjectType.NonObject)
                    {
                        Worked = true;
                    }
                    else if (SetVersion())
                    {
                        // Actual object, version set correctly
                        Worked = true;
                    }
                    else
                    {
                        // Something happened
                        Worked = false;
                    }
                }
                catch (Exception ex)
                {
                    _err = ex.ToString();
                    _SQLOutput.Add(ex.ToString());
                }
            }
            else
            {
                Worked = true;
            }

            SetDBVersion();

            return Worked;

        }

        #region Install Helpers

        /// <summary>
        /// Uses SMO to do its work.  I hate SMO.
        /// Just puts the version information in the extended properties.
        /// </summary>
        /// <returns></returns>
        private bool SetVersion()
        {
            bool Worked = false;

            ServerConnection conn = new ServerConnection(_conn);
            Server srv = new Server(conn);

            ExtendedProperty Maj = new ExtendedProperty();
            ExtendedProperty Min = new ExtendedProperty();
            ExtendedProperty Auth = new ExtendedProperty();
            ExtendedProperty LastUpdate = new ExtendedProperty();
            ExtendedProperty AppVersion = new ExtendedProperty();

            Maj.Name = AUU_MajVer;
            Min.Name = AUU_MinVer;

            Auth.Name = AUU_Author;
            LastUpdate.Name = AUU_ObjTime;
            AppVersion.Name = AUU_AppVersion;

            try
            {
                switch (_type)
                {
                    case ObjectType.Procedure:
                        Maj.Parent = srv.Databases[_Database].StoredProcedures[_ObjectName, _ObjectSchema];
                        Min.Parent = srv.Databases[_Database].StoredProcedures[_ObjectName, _ObjectSchema];
                        Auth.Parent = srv.Databases[_Database].StoredProcedures[_ObjectName, _ObjectSchema];
                        LastUpdate.Parent = srv.Databases[_Database].StoredProcedures[_ObjectName, _ObjectSchema];
                        AppVersion.Parent = srv.Databases[_Database].StoredProcedures[_ObjectName, _ObjectSchema];
                        break;
                    case ObjectType.View:
                        Maj.Parent = srv.Databases[_Database].Views[_ObjectName, _ObjectSchema];
                        Min.Parent = srv.Databases[_Database].Views[_ObjectName, _ObjectSchema];
                        Auth.Parent = srv.Databases[_Database].Views[_ObjectName, _ObjectSchema];
                        LastUpdate.Parent = srv.Databases[_Database].Views[_ObjectName, _ObjectSchema];
                        AppVersion.Parent = srv.Databases[_Database].Views[_ObjectName, _ObjectSchema];
                        break;
                    case ObjectType.Table:
                        Maj.Parent = srv.Databases[_Database].Tables[_ObjectName, _ObjectSchema];
                        Min.Parent = srv.Databases[_Database].Tables[_ObjectName, _ObjectSchema];
                        Auth.Parent = srv.Databases[_Database].Tables[_ObjectName, _ObjectSchema];
                        LastUpdate.Parent = srv.Databases[_Database].Tables[_ObjectName, _ObjectSchema];
                        AppVersion.Parent = srv.Databases[_Database].Tables[_ObjectName, _ObjectSchema];
                        break;
                    case ObjectType.Function:
                        Maj.Parent = srv.Databases[_Database].UserDefinedFunctions[_ObjectName, _ObjectSchema];
                        Min.Parent = srv.Databases[_Database].UserDefinedFunctions[_ObjectName, _ObjectSchema];
                        Auth.Parent = srv.Databases[_Database].UserDefinedFunctions[_ObjectName, _ObjectSchema];
                        LastUpdate.Parent = srv.Databases[_Database].UserDefinedFunctions[_ObjectName, _ObjectSchema];
                        AppVersion.Parent = srv.Databases[_Database].UserDefinedFunctions[_ObjectName, _ObjectSchema];
                        break;
                }

                Maj.Value = _MajorVersion;
                Min.Value = _MinorVersion;
                Maj.Create();
                Min.Create();

                Auth.Value = _Author;
                Auth.Create();

                LastUpdate.Value = System.DateTime.Now.ToString();
                LastUpdate.Create();

                AppVersion.Value = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                AppVersion.Create();

                Worked = true;
            }
            catch
            {
                Worked = false;
            }

            return Worked;
        }

        /// <summary>
        /// Stamps the database with the most current time and the current version of the AUU
        /// All handled in SQL extended properties
        /// </summary>
        private void SetDBVersion()
        {

            ServerConnection conn = new ServerConnection(_conn);
            Server srv = new Server(conn);

            ExtendedProperty AppVersion = new ExtendedProperty();
            ExtendedProperty DBTime = new ExtendedProperty();
            bool AlterVer = false;
            bool AlterTime = false;

            AppVersion.Name = AUU_AppVersion;
            DBTime.Name = AUU_DBTime;

            try
            {
                foreach (ExtendedProperty t in srv.Databases[_Database].ExtendedProperties)
                {
                    if (t.Name == DBTime.Name)
                    {
                        t.Value = System.DateTime.Now.ToString();
                        t.Alter();
                        AlterTime = true;
                    }

                    if (t.Name == AppVersion.Name)
                    {
                        t.Value = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                        t.Alter();
                        AlterVer = true;
                    }
                }

                AppVersion.Parent = srv.Databases[_Database];
                DBTime.Parent = srv.Databases[_Database];

                AppVersion.Value = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                DBTime.Value = System.DateTime.Now.ToString();

                if (!AlterTime)
                {
                    DBTime.Create();
                }

                if (!AlterVer)
                {
                    AppVersion.Create();
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("** Failed to update extended properties on database " + _Database);
                System.Console.WriteLine(ex.ToString());
            }

        }

        /// <summary>
        /// Big method that queries everything and finds the version of the currently installed object
        /// </summary>
        /// <returns></returns>
        private bool ObjectShouldBeInstalled()
        {
            /*
             * 1. Test to see if object exists
             * 2. Find the current version
             */
            bool ShouldInstall = true;
            Version ver = new Version();

            // See if the object exists
            switch (_type)
            {
                case ObjectType.Procedure:
                    ver = ProcedureExists();
                    break;
                case ObjectType.View:
                    ver = ViewExists();
                    break;
                case ObjectType.Table:
                    ver = TableExists();
                    break;
                case ObjectType.Function:
                    ver = FunctionExists();
                    break;
                case ObjectType.NonObject:
                    ver.Exists = false;
                    break;
                default:
                    ver.Exists = false;
                    break;
            }

            // OK, we know it exists.  Now find out its version.
            if (ver.Exists)
            {
                if (ver.Match(_MajorVersion, _MinorVersion))
                {
                    ShouldInstall = false;
                }
            }

            return ShouldInstall;
        }

        #region The Lame-o *Exists() methods
        private Version ProcedureExists()
        {
            Version v = new Version();
            v.Exists = false;

            ServerConnection conn = new ServerConnection(_conn);
            Server server = new Server(conn);

            foreach (StoredProcedure p in server.Databases[_Database].StoredProcedures)
            {
                if ((p.Name.ToUpper() == _ObjectName.ToUpper()) && (p.Schema.ToUpper() == _ObjectSchema.ToUpper()))
                {
                    v.Exists = true;
                    foreach (ExtendedProperty property in p.ExtendedProperties)
                    {
                        if (property.Name == "AUU_Major_Version_Number")
                        {
                            v.MajorVersion = int.Parse(property.Value.ToString());
                        }
                        else if (property.Name == "AUU_Minor_Version_Number")
                        {
                            v.MinorVersion = int.Parse(property.Value.ToString());
                        }
                    }
                }
            }

            return v;
        }

        private Version FunctionExists()
        {
            Version v = new Version();
            v.Exists = false;

            ServerConnection conn = new ServerConnection(_conn);
            Server server = new Server(conn);

            foreach (UserDefinedFunction f in server.Databases[_Database].UserDefinedFunctions)
            {
                if ((f.Name.ToUpper() == _ObjectName.ToUpper()) && (f.Schema.ToUpper() == _ObjectSchema.ToUpper()))
                {
                    v.Exists = true;
                    foreach (ExtendedProperty property in f.ExtendedProperties)
                    {
                        if (property.Name == "AUU_Major_Version_Number")
                        {
                            v.MajorVersion = int.Parse(property.Value.ToString());
                        }
                        else if (property.Name == "AUU_Minor_Version_Number")
                        {
                            v.MinorVersion = int.Parse(property.Value.ToString());
                        }
                    }
                }
            }

            return v;
        }

        private Version ViewExists()
        {
            Version v = new Version();
            v.Exists = false;

            ServerConnection conn = new ServerConnection(_conn);
            Server server = new Server(conn);

            foreach (View vw in server.Databases[_Database].Views)
            {
                if ((vw.Name.ToUpper() == _ObjectName.ToUpper()) && (vw.Schema.ToUpper() == _ObjectSchema.ToUpper()))
                {
                    v.Exists = true;
                    foreach (ExtendedProperty property in vw.ExtendedProperties)
                    {
                        if (property.Name == "AUU_Major_Version_Number")
                        {
                            v.MajorVersion = int.Parse(property.Value.ToString());
                        }
                        else if (property.Name == "AUU_Minor_Version_Number")
                        {
                            v.MinorVersion = int.Parse(property.Value.ToString());
                        }
                    }
                }
            }

            return v;
        }

        private Version TableExists()
        {
            Version v = new Version();
            v.Exists = false;

            ServerConnection conn = new ServerConnection(_conn);
            Server server = new Server(conn);

            foreach (Table t in server.Databases[_Database].Tables)
            {
                if ((t.Name.ToUpper() == _ObjectName.ToUpper()) && (t.Schema.ToUpper() == _ObjectSchema.ToUpper()))
                {
                    v.Exists = true;
                    foreach (ExtendedProperty property in t.ExtendedProperties)
                    {
                        if (property.Name == "AUU_Major_Version_Number")
                        {
                            v.MajorVersion = int.Parse(property.Value.ToString());
                        }
                        else if (property.Name == "AUU_Minor_Version_Number")
                        {
                            v.MinorVersion = int.Parse(property.Value.ToString());
                        }
                    }
                }
            }

            return v;
        }
        #endregion *Exists()

        #endregion Install Helpers

        #region Internal Helper junk

        /// <summary>
        /// Tests this method to see if it's OK to be installed
        /// </summary>
        private void RunReadyTest()
        {
            if (_type == ObjectType.NonObject)
            {
                // different rules if this is some ad-hoc crap
                if ((!string.IsNullOrEmpty(_ObjectDefinition)) &&
                    (!string.IsNullOrEmpty(_Database)) &&
                    (_conn != null))
                {
                    if (_conn.State == System.Data.ConnectionState.Open)
                    {
                        _IsReadyToBeInstalled = true;
                    }
                    else
                    {
                        _err = "SQL connection isn't ready for use";
                        _IsReadyToBeInstalled = false;
                    }
                }
            }
            else
            {
                if ((!string.IsNullOrEmpty(_ObjectDefinition)) &&
                    (!string.IsNullOrEmpty(_ObjectName)) &&
                    (!string.IsNullOrEmpty(_ObjectSchema)) &&
                    (!string.IsNullOrEmpty(_Database)) &&
                    (_conn != null) &&
                    (_type != ObjectType.NotSet))
                {
                    if (_conn.State == System.Data.ConnectionState.Open)
                    {
                        _IsReadyToBeInstalled = true;
                    }
                    else
                    {
                        _err = "SQL connection isn't ready for use";
                        _IsReadyToBeInstalled = false;
                    }
                }
                else
                {
                    _IsReadyToBeInstalled = false;
                }
            }
        }

        /// <summary>
        /// Brings us back to the base state
        /// </summary>
        private void ResetClass()
        {
            if (_conn != null)
            {
                _conn.Dispose();
            }
            _ObjectDefinition = "";
            _MajorVersion = -1;
            _MinorVersion = -1;
            _Author = "";
            _Database = "";
            _conn = null;
            _ObjectName = "";
            _ObjectSchema = "";
            _err = "Object is not ready to be installed";
            _SQLOutput = new ArrayList();
            _type = ObjectType.NotSet;
            this.RunReadyTest();
        }

        private void _conn_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            _SQLOutput.Add(e.ToString());
        }

        #endregion Internal Helper Junk

        #region IDisposable Members

        public void Dispose()
        {
            this.ResetClass();
        }

        #endregion
    }
}
