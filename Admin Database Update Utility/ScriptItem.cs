using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace SQLSlayer.DBA.AdminUpdateUtility
{
    [Serializable]
    public class ScriptItem
    {
        [XmlAttribute("name")]
        public string Name;
        [XmlAttribute("environment")]
        public string Environment;
        [XmlAttribute("groups")]
        public string Groups;
        [XmlAttribute("MajorVersion")]
        public string MajorVersion;
        [XmlAttribute("MinorVersion")]
        public string MinorVersion;
        [XmlAttribute("database")]
        public string Database;
        [XmlAttribute("author")]
        public string Author;
        [XmlAttribute("location")]
        public string Location;
        [XmlAttribute("type")]
        public string Type;
        [XmlAttribute("buildMin")]
        public string BuildMin;
        [XmlAttribute("buildMax")]
        public string BuildMax;
    }

    //[Serializable]
    //public class ScriptBasePath
    //{
    //    [XmlAttribute("basepath")]
    //    public string BasePath;
    //}
}
