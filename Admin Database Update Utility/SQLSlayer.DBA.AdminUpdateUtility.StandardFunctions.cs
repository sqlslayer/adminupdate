using System;
using System.Collections;
using System.Text;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace SQLSlayer.DBA.AdminUpdateUtility
{

    public static class StandardObjects
    {
        public class ServerEnvironment
        {
            public string ServerName;
            public string SQLVersion;
            public bool UseTrusted;
            public string User;
            public string Pass;

            public ServerEnvironment()
            {
                UseTrusted = true;
                User = "";
                Pass = "";
            }

            private ArrayList _Groups = new ArrayList();

            /// <summary>
            /// Adds a comma separated list of group values to this server
            /// </summary>
            /// <param name="GroupList"></param>
            public void AddGroups(string GroupList)
            {
                if (!String.IsNullOrEmpty(GroupList))
                {
                    foreach (string grp in GroupList.Split(','))
                    {
                        if (grp == "*")
                        {
                            _Groups.Clear();
                            break;
                        }
                        else
                        {
                            _Groups.Add(grp);
                        }
                    }
                }
            }

            /// <summary>
            /// Returns an array of groups specific to this server
            /// </summary>
            public string[] Groups
            {
                get
                {
                    string[] s = new string[_Groups.Count];

                    for (int i = 0; i < _Groups.Count; i++)
                    {
                        s[i] = _Groups[i].ToString();
                    }
                    return s;
                }
            }

            /// <summary>
            /// Provided the list of object groups, this will decide if the group lists match
            /// </summary>
            /// <param name="ObjectGroupList"></param>
            /// <returns></returns>
            public bool IsPartOfGroup(string[] ObjectGroupList)
            {
                bool Match = false;

                // If the object doesn't have any groups defined then that is a match
                if (ObjectGroupList.Length == 0)
                {
                    return true;
                }

                // If the server doesn't have any groups defined then that is a match
                if (this.Groups.Length == 0)
                {
                    return true;
                }

                // OK, both sides have a set of groups, so see if we can find a match in there somewhere
                foreach (string objGroup in ObjectGroupList)
                {
                    foreach (string srvGroup in this.Groups)
                    {
                        if (objGroup.ToUpper() == srvGroup.ToUpper())
                        {
                            Match = true;
                            break;
                        }
                    }
                    if (Match)
                    {
                        break;
                    }
                }
                return Match;
            }
        }
    }

    /// <summary>
    /// Holds some general types of functions...
    /// </summary>
    public static class StandardFunctions
    {

        /// <summary>
        /// Class used to parse out the build numbers into their components
        /// </summary>
        private class SQLBuildNumber
        {
            private string _FullBuild;

            public SQLBuildNumber(string FullBuildNumber)
            {
                _FullBuild = FullBuildNumber;
            }

            public bool IsGreaterThan(SQLBuildNumber CompareTo)
            {
                if (this.Major > CompareTo.Major)
                {
                    return true;
                }
                else if (this.Major < CompareTo.Major)
                {
                    return false;
                }
                else
                {
                    if (this.Minor > CompareTo.Minor)
                    {
                        return true;
                    }
                    else if (this.Minor < CompareTo.Minor)
                    {
                        return false;
                    }
                    else
                    {
                        if (this.Build > CompareTo.Build)
                        {
                            return true;
                        }
                        else if (this.Build < CompareTo.Build)
                        {
                            return false;
                        }
                        else
                        {
                            if (this.Extra > CompareTo.Extra)
                            {
                                return true;
                            }
                            else if (this.Extra < CompareTo.Extra)
                            {
                                return false;
                            }
                            else
                            {
                                // They are identical.  Not Greater Than
                                return false;
                            }
                        }
                    }
                }
            }

            #region Public Properties
            /// <summary>
            /// Returns the first number in the Major.Minor.Build.Extra version number
            /// </summary>
            public int Major
            {
                get
                {
                    if (string.IsNullOrEmpty(_FullBuild))
                    {
                        return 0;
                    }
                    else
                    {
                        if (_FullBuild.Split('.').Length > 0)
                        {
                            return Int32.Parse(_FullBuild.Split('.')[0]);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
            }

            /// <summary>
            /// Returns the second number in the Major.Minor.Build.Extra version number
            /// </summary>
            public int Minor
            {
                get
                {
                    if (string.IsNullOrEmpty(_FullBuild))
                    {
                        return 0;
                    }
                    else
                    {
                        if (_FullBuild.Split('.').Length > 1)
                        {
                            return Int32.Parse(_FullBuild.Split('.')[1]);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
            }

            /// <summary>
            /// Returns the third number in the Major.Minor.Build.Extra version number
            /// </summary>
            public int Build
            {
                get
                {
                    if (string.IsNullOrEmpty(_FullBuild))
                    {
                        return 0;
                    }
                    else
                    {
                        if (_FullBuild.Split('.').Length > 2)
                        {
                            return Int32.Parse(_FullBuild.Split('.')[2]);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
            }

            /// <summary>
            /// Returns the fourth number in the Major.Minor.Build.Extra version number
            /// </summary>
            public int Extra
            {
                get
                {
                    if (string.IsNullOrEmpty(_FullBuild))
                    {
                        return 0;
                    }
                    else
                    {
                        if (_FullBuild.Split('.').Length > 3)
                        {
                            return Int32.Parse(_FullBuild.Split('.')[3]);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }
            }

            public bool IsEmpty
            {
                get
                {
                    return string.IsNullOrEmpty(_FullBuild);
                }
            }

            #endregion

            #region Overrides
            public override bool Equals(object obj)
            {
                SQLBuildNumber b;

                try
                {
                    b = (SQLBuildNumber)obj;
                }
                catch
                {
                    return false;
                }

                if ((this.Major == b.Major) &&
                    (this.Minor == b.Minor) &&
                    (this.Build == b.Build) &&
                    (this.Extra == b.Extra))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            #endregion
        }

        /// <summary>
        /// De-constructs the version number (build) of SQL server and compares it to the high-low values from the configuration
        /// </summary>
        /// <param name="CfgLowBuild"></param>
        /// <param name="CfgHighBuild"></param>
        /// <param name="MeasuredBuild"></param>
        /// <returns>True if the version is within the configured range</returns>
        public static bool IsBuildWithinRange(string CfgLowBuild, string CfgHighBuild, string MeasuredBuild)
        {

            SQLBuildNumber LowNum = new SQLBuildNumber(CfgLowBuild);
            SQLBuildNumber HighNum = new SQLBuildNumber(CfgHighBuild);
            SQLBuildNumber SQLNum = new SQLBuildNumber(MeasuredBuild);

            if ((LowNum.IsEmpty == false) && (HighNum.IsEmpty == false))
            {
                // If both values are present, make sure that the range is correct (low < high)
                if (LowNum.IsGreaterThan(HighNum))
                {
                    throw new Exception("Low build has to be lower than the high build");
                }
            }

            if (LowNum.IsEmpty && HighNum.IsEmpty)
            {
                return true;
            }
            else if (SQLNum.Equals(LowNum) || SQLNum.Equals(HighNum))
            {
                return true;
            }
            else if ((SQLNum.IsGreaterThan(LowNum)) && (HighNum.IsEmpty))
            {
                return true;
            }
            else if ((HighNum.IsGreaterThan(SQLNum)) && (LowNum.IsEmpty))
            {
                return true;
            }
            else if ((HighNum.IsGreaterThan(SQLNum)) && (SQLNum.IsGreaterThan(LowNum)))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Gets the first and second columns defined by the SelectCmd.  
        /// Absolutely no error handling.  Do that yourself.
        /// </summary>
        /// <param name="conn">An _OPEN_ connection to SQL</param>
        /// <param name="SelectCmd">A single-batch T-SQL command that returns at least two column in the recordset</param>
        /// <returns>StandardObjects.ServerEnvironment[] object</returns>
        public static StandardObjects.ServerEnvironment[] GetServerEnvironmentFromSQL(System.Data.SqlClient.SqlConnection conn, string SelectCmd)
        {

            ServerConnection _conn = new ServerConnection(conn);
            Server _srv = new Server(_conn);
            ArrayList srvrs = new ArrayList();
            StandardObjects.ServerEnvironment[] srv_out;
            StandardObjects.ServerEnvironment env;
            string _user;
            string _pass;

            System.Data.SqlClient.SqlDataReader rdr = _srv.ConnectionContext.ExecuteReader(SelectCmd);

            while (rdr.Read())
            {
                env = new StandardObjects.ServerEnvironment();
                env.ServerName = rdr.GetString(0);
                env.SQLVersion = rdr.GetInt32(1).ToString();

                _user = "";
                _pass = "";

                if (rdr.IsDBNull(2) == false)
                {
                    _user = rdr.GetString(2);
                    _pass = rdr.GetString(3);
                }

                // Add the user/pass info
                if (String.IsNullOrEmpty(_user) == true)
                {
                    env.UseTrusted = true;
                    env.User = "";
                    env.Pass = "";
                }
                else
                {
                    env.UseTrusted = false;
                    env.User = _user;
                    env.Pass = _pass;
                }
                
                // Get the group data -- if provided
                try
                {
                    env.AddGroups(rdr.GetString(4));
                }
                catch (System.IndexOutOfRangeException)
                {
                    env.AddGroups("");
                }

                srvrs.Add(env);
            }

            rdr.Close();
            rdr.Dispose();

            srv_out = (StandardObjects.ServerEnvironment[])(srvrs.ToArray(typeof(StandardObjects.ServerEnvironment)));

            return srv_out;

        }

        public static AdminSQLObject.ObjectType ConvertLetterToType(string Letter)
        {
            switch (Letter)
            {
                case "P":
                    return AdminSQLObject.ObjectType.Procedure;
                case "V":
                    return AdminSQLObject.ObjectType.View;
                case "T":
                    return AdminSQLObject.ObjectType.Table;
                case "F":
                    return AdminSQLObject.ObjectType.Function;
                case "Q":
                    return AdminSQLObject.ObjectType.NonObject;
                default:
                    return AdminSQLObject.ObjectType.NotSet;
            }
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="MessageLogLevel"></param>
        /// <param name="AppLogLevel"></param>
        public static void LogWriter(string Message, int MessageLogLevel, int AppLogLevel)
        {
            if (MessageLogLevel <= AppLogLevel)
            {
                System.Console.WriteLine(Message);
            }
        }
    }

    
}
