USE admin
GO
-- OEC Servers
INSERT INTO [dbo].[ServerInventory_Master] (ServerName, InstanceName, PortNumber, SQLVersion, EnvironmentID, EditionID, Description)
SELECT 
	CASE WHEN CHARINDEX('\',old.ServerName) = 0 THEN old.ServerName
		ELSE SUBSTRING(old.ServerName,0,CHARINDEX('\',old.ServerName))
	END as ServerNameBase
	,CASE WHEN CHARINDEX('\',old.ServerName) = 0 THEN NULL
		WHEN CHARINDEX(',',old.ServerName) > 0 THEN SUBSTRING(old.ServerName,CHARINDEX('\',old.ServerName) + 1,CHARINDEX(',',old.ServerName) - CHARINDEX('\',old.ServerName) - 1)
		ELSE SUBSTRING(old.ServerName,CHARINDEX('\',old.ServerName) + 1,100)
	END as InstanceName
	,CASE WHEN CHARINDEX(',',old.ServerName) = 0 THEN NULL
		ELSE SUBSTRING(old.ServerName,CHARINDEX(',',old.ServerName) + 1,100)
	END as PortNumber
	,old.[SQLVersion]
	,env.[EnvironmentID]
	,ed.[EditionID]
	,old.[Description]
FROM [dbo].[OLD_OECServerNames] old
INNER JOIN [dbo].[ServerInventory_Environments] env
	ON env.EnvironmentName = old.Environment
INNER JOIN [dbo].[ServerInventory_Editions] ed
	ON ed.EditionName = replace(replace(old.Edition,'[','('),']',')')
	
-- Savvis Servers
INSERT INTO [dbo].[ServerInventory_Master] (ServerName, InstanceName, PortNumber, SQLVersion, EnvironmentID, EditionID, Description)
SELECT 
	CASE WHEN CHARINDEX('\',old.ServerName) = 0 THEN old.ServerName
		ELSE SUBSTRING(old.ServerName,0,CHARINDEX('\',old.ServerName))
	END as ServerNameBase
	,CASE WHEN CHARINDEX('\',old.ServerName) = 0 THEN NULL
		WHEN CHARINDEX(',',old.ServerName) > 0 THEN SUBSTRING(old.ServerName,CHARINDEX('\',old.ServerName) + 1,CHARINDEX(',',old.ServerName) - CHARINDEX('\',old.ServerName) - 1)
		ELSE SUBSTRING(old.ServerName,CHARINDEX('\',old.ServerName) + 1,100)
	END as InstanceName
	,CASE WHEN CHARINDEX(',',old.ServerName) = 0 THEN NULL
		ELSE SUBSTRING(old.ServerName,CHARINDEX(',',old.ServerName) + 1,100)
	END as PortNumber
	,old.[SQLVersion]
	,env.[EnvironmentID]
	,ed.[EditionID]
	,NULL as [Description]
FROM [dbo].[OLD_ProdServerNames] old
INNER JOIN [dbo].[ServerInventory_Environments] env
	ON env.EnvironmentName = old.Environment
INNER JOIN [dbo].[ServerInventory_Editions] ed
	ON ed.EditionName = replace(replace(old.Edition,'[','('),']',')')
	
-- InstaceNames
INSERT INTO [dbo].[ServerInventory_AttributeList] (ServerID, AttribName, AttribValue)
SELECT 
	m.ServerID
	,'InstanceName'
	,si.InstanceName
FROM dbo.OLD_OECServerNames sn
INNER JOIN dbo.OLD_ServerInstanceNames si
	ON si.ServerID = sn.ServerID
	AND si.IsProd = 0
INNER JOIN [dbo].[ServerInventory_AllServers_vw] m
	ON m.FullName = sn.ServerName
UNION
SELECT 
	m.ServerID
	,'InstanceName'
	,si.InstanceName
FROM dbo.OLD_ProdServerNames sn
INNER JOIN dbo.OLD_ServerInstanceNames si
	ON si.ServerID = sn.ServerID
	AND si.IsProd = 1
INNER JOIN [dbo].[ServerInventory_AllServers_vw] m
	ON m.FullName = sn.ServerName


