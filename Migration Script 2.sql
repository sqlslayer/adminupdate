USE admin
GO

SET IDENTITY_INSERT [dbo].[ServerInventory_SQL_ServerCredentials] ON
INSERT INTO [dbo].[ServerInventory_SQL_ServerCredentials] ([CredentialID], [UserName], [Password])
SELECT [CredentialID], [UserName], [Password]
FROM dbo.ServerInventory_ServerCredentials
SET IDENTITY_INSERT [dbo].[ServerInventory_SQL_ServerCredentials] OFF

SET IDENTITY_INSERT [dbo].[ServerInventory_SQL_Master] ON
INSERT INTO [dbo].[ServerInventory_SQL_Master] ([ServerID], [ServerName], [InstanceName], [PortNumber], [Description], [SQLVersion], [Enabled], [EnvironmentID], [EditionID], [UseCredential], [CredentialID])
SELECT [ServerID], [ServerName], [InstanceName], [PortNumber], [Description], [SQLVersion], [Enabled], [EnvironmentID], [EditionID], [UseCredential], [CredentialID]
FROM [dbo].[ServerInventory_Master]
SET IDENTITY_INSERT [dbo].[ServerInventory_SQL_Master] OFF

INSERT INTO [dbo].[ServerInventory_SQL_AttributeList] ([ServerID], [AttribName], [AttribValue])
SELECT [ServerID], [AttribName], [AttribValue]
FROM [dbo].[ServerInventory_AttributeList]