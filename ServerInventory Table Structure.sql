USE [admin]
GO

/****** Object:  Table [dbo].[OECServerNames]    Script Date: 09/23/2008 08:35:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF OBJECT_ID('[dbo].[ServerInventory_SQL_AttributeList]') IS NOT NULL
	DROP TABLE [dbo].[ServerInventory_SQL_AttributeList]

IF OBJECT_ID('[dbo].[ServerInventory_SQL_Master]') IS NOT NULL
	DROP TABLE [dbo].[ServerInventory_SQL_Master]

IF OBJECT_ID('[dbo].[ServerInventory_SQL_ServerCredentials]') IS NOT NULL
	DROP TABLE [dbo].[ServerInventory_SQL_ServerCredentials]

IF OBJECT_ID('[dbo].[ServerInventory_Environments]') IS NOT NULL
	DROP TABLE [dbo].[ServerInventory_Environments]
	
IF OBJECT_ID('[dbo].[ServerInventory_SQL_Editions]') IS NOT NULL
	DROP TABLE [dbo].[ServerInventory_SQL_Editions]
	
IF OBJECT_ID('[dbo].[ServerInventory_SQL_ServerDBTableIDs]') IS NOT NULL
	DROP TABLE [dbo].[ServerInventory_SQL_ServerDBTableIDs]
	
IF OBJECT_ID('[dbo].[ServerInventory_SQL_TableIDs]') IS NOT NULL
	DROP TABLE [dbo].[ServerInventory_SQL_TableIDs]
	
IF OBJECT_ID('[dbo].[ServerInventory_SQL_DatabaseIDs]') IS NOT NULL
	DROP TABLE [dbo].[ServerInventory_SQL_DatabaseIDs]

IF OBJECT_ID('[dbo].[ServerInventory_ServerIDs]') IS NOT NULL
	DROP TABLE [dbo].[ServerInventory_ServerIDs]


--------------------------
GO
CREATE TABLE [dbo].[ServerInventory_SQL_ServerCredentials] (
	[CredentialID]		INT IDENTITY PRIMARY KEY CLUSTERED,
	[UserName]			NVARCHAR(100),
	[Password]			NVARCHAR(500)
)

GO
CREATE TABLE [dbo].[ServerInventory_Environments] (
	[EnvironmentID]		TINYINT IDENTITY PRIMARY KEY NONCLUSTERED,
	[EnvironmentName]	VARCHAR(100)
)

GO
CREATE TABLE [dbo].[ServerInventory_SQL_Editions] (
	[EditionID]			SMALLINT IDENTITY PRIMARY KEY NONCLUSTERED,
	[EditionName]		VARCHAR(100)
)

GO
CREATE TABLE [dbo].[ServerInventory_SQL_Master](
	[ServerID]			INT IDENTITY(1,1) PRIMARY KEY NONCLUSTERED,
	[ServerName]		VARCHAR(100) NOT NULL,
	[InstanceName]		VARCHAR(100) NULL,
	[PortNumber]		INT NULL,
	[Description]		NVARCHAR(max) NULL,
	[SQLVersion]		INT NOT NULL,
	[Enabled]			BIT NOT NULL DEFAULT (1),
	
	[EnvironmentID]		TINYINT NOT NULL CONSTRAINT FK_SI_Environments_EnvironmentID REFERENCES [dbo].[ServerInventory_Environments] ([EnvironmentID]),
	[EditionID]			SMALLINT NOT NULL CONSTRAINT FK_SI_SQL_Editions_EditionID REFERENCES [dbo].[ServerInventory_SQL_Editions] ([EditionID]),
	[UseCredential]		BIT DEFAULT (0),			-- If 1 then use the CredentialID
	[CredentialID]		INT NULL CONSTRAINT FK_SI_SQL_ServerCredentials_CredentialID REFERENCES [dbo].[ServerInventory_SQL_ServerCredentials] ([CredentialID])
) ON [PRIMARY]

GO
CREATE TABLE [dbo].[ServerInventory_SQL_AttributeList] (
	[AttribID]			INT IDENTITY PRIMARY KEY NONCLUSTERED,
	[ServerID]			INT CONSTRAINT FK_SI_SQL_Master_ServerID FOREIGN KEY REFERENCES [dbo].[ServerInventory_SQL_Master] ([ServerID]) ON DELETE CASCADE,
	[AttribName]		VARCHAR(100),
	[AttribValue]		NVARCHAR(1000)
)

GO
-----------------

CREATE TABLE [dbo].[ServerInventory_ServerIDs] (
	[ServerID]			INT IDENTITY PRIMARY KEY NONCLUSTERED
	,[ServerName]		VARCHAR(200)
)
GO

CREATE TABLE [dbo].[ServerInventory_SQL_DatabaseIDs] (
	[DatabaseID]		INT IDENTITY PRIMARY KEY NONCLUSTERED
	,[DBName]			SYSNAME
)
GO

CREATE TABLE [dbo].[ServerInventory_SQL_TableIDs] (
	[TableID]			INT IDENTITY PRIMARY KEY NONCLUSTERED
	,[SchemaName]		SYSNAME
	,[TableName]		SYSNAME
)
GO

CREATE TABLE [dbo].[ServerInventory_SQL_ServerDBTableIDs] (
	[ServerDBTableID]	INT IDENTITY PRIMARY KEY NONCLUSTERED
	,[ServerID]			INT CONSTRAINT [FK_SI_ServerIDs_ServerID] FOREIGN KEY REFERENCES [dbo].[ServerInventory_ServerIDs] ([ServerID])
	,[DatabaseID]		INT CONSTRAINT [FK_SI_SQL_DatabaseIDs_DatabaseID] FOREIGN KEY REFERENCES [dbo].[ServerInventory_SQL_DatabaseIDs] ([DatabaseID])
	,[TableID]			INT CONSTRAINT [FK_SI_SQL_TableIDs_TableID] FOREIGN KEY REFERENCES [dbo].[ServerInventory_SQL_TableIDs] ([TableID])
	,[LastUpdated]		SMALLDATETIME
)

GO
CREATE CLUSTERED INDEX IX_SpaceUsed_ServerDBTableIDs_Main ON [dbo].[ServerInventory_SQL_ServerDBTableIDs] (ServerID,DatabaseID,TableID)

GO
-------------------------------
-- Load control data

INSERT INTO [dbo].[ServerInventory_SQL_Editions] ([EditionName])
SELECT 'Developer Edition'
UNION
SELECT 'Developer Edition (64-bit)'
UNION
SELECT 'Enterprise Edition'
UNION
SELECT 'Enterprise Edition (64-bit)'
UNION
SELECT 'Standard Edition'
UNION
SELECT 'Standard Edition (64-bit)'


INSERT INTO [dbo].[ServerInventory_Environments] ([EnvironmentName])
SELECT 'BI'
UNION
SELECT 'DEV'
UNION
SELECT 'OECPROD'
UNION
SELECT 'QA'
UNION
SELECT 'SAVVIS'

----------------------------
-- Create a view to assemble the data
GO
IF OBJECT_ID('[dbo].[ServerInventory_SQL_AllServers_vw]') IS NOT NULL
	DROP VIEW [dbo].[ServerInventory_SQL_AllServers_vw]
GO

CREATE VIEW [dbo].[ServerInventory_SQL_AllServers_vw]
AS
SELECT 
	m.[ServerID]
	,m.[ServerName]
	,m.[InstanceName]
	,m.[PortNumber]
	,CASE 
		WHEN m.[InstanceName] IS NOT NULL AND m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + '\' + m.[InstanceName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		WHEN m.[InstanceName] IS NOT NULL
			THEN m.[ServerName] + '\' + m.[InstanceName]
		WHEN m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		ELSE m.[ServerName]
	END as [FullName]
	,m.[SQLVersion]
	,env.[EnvironmentName]	as Environment
	,ed.[EditionName]		as Edition
	,m.[Description]
	,m.[UseCredential]
	,cred.[UserName]
	,cred.[Password]
	,'Data Source=' + CASE 
		WHEN m.[InstanceName] IS NOT NULL AND m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + '\' + m.[InstanceName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		WHEN m.[InstanceName] IS NOT NULL
			THEN m.[ServerName] + '\' + m.InstanceName
		WHEN m.[PortNumber] IS NOT NULL
			THEN m.[ServerName] + ',' + CAST(m.[PortNumber] AS VARCHAR(10))
		ELSE m.[ServerName]
	END + ';Initial Catalog=master;' + CASE
		WHEN m.[UseCredential] = 0 
			THEN 'Integrated Security=SSPI;'
		ELSE ';User Id=' + cred.[UserName] + ';Password=' + cred.[Password] + ';'
	END AS [DotNetConnectionString]
FROM [dbo].[ServerInventory_SQL_Master] m
INNER JOIN [dbo].[ServerInventory_Environments] env
	ON env.[EnvironmentID] = m.[EnvironmentID]
INNER JOIN [dbo].[ServerInventory_SQL_Editions] ed
	ON ed.[EditionID] = m.[EditionID]
LEFT OUTER JOIN [dbo].[ServerInventory_SQL_ServerCredentials] cred
	ON cred.[CredentialID] = m.[CredentialID]
	
GO
IF OBJECT_ID('[dbo].[ServerInventory_SQL_AllServers_Compatibility_vw]') IS NOT NULL
	DROP VIEW [dbo].[ServerInventory_SQL_AllServers_Compatibility_vw]
GO

CREATE VIEW [dbo].[ServerInventory_SQL_AllServers_Compatibility_vw]
AS

SELECT
	m.[ServerID]
	,m.[FullName]			AS ServerName
	,m.[SQLVersion]
	,m.[Environment]
	,m.[Edition]
	,m.[Description]
FROM [dbo].[ServerInventory_SQL_AllServers_vw] m

GO
IF OBJECT_ID('[dbo].[ServerInventory_SQL_ServerAttributes_vw]') IS NOT NULL
	DROP VIEW [dbo].[ServerInventory_SQL_ServerAttributes_vw]
GO


CREATE VIEW [dbo].[ServerInventory_SQL_ServerAttributes_vw]
AS

SELECT 
	m.[ServerID]
	,m.[FullName]				AS ServerName
	,m.[SQLVersion]
	,m.[Environment]			AS EnvironmentName
	,attrib.[AttribName]
	,attrib.[AttribValue]
FROM [dbo].[ServerInventory_SQL_AllServers_vw] m
INNER JOIN [dbo].[ServerInventory_SQL_AttributeList] attrib
	ON attrib.[ServerID] = m.[ServerID]
	
GO
IF OBJECT_ID('[dbo].[ServerInventory_SQL_ServerInstances_vw]') IS NOT NULL
	DROP VIEW [dbo].[ServerInventory_SQL_ServerInstances_vw]
GO

CREATE VIEW [dbo].[ServerInventory_SQL_ServerInstances_vw]
AS

WITH SN ([ServerName], [Environment], [InstanceName])
AS
(
	SELECT 
		[ServerName]
		,[EnvironmentName]
		,[AttribValue]
	FROM [dbo].[ServerInventory_SQL_ServerAttributes_vw] 
	WHERE [AttribName] = 'InstanceName'
)
SELECT 
	i.[InstanceName]
	,d.[ServerName] as DEV
	,q.[ServerName] as QA
	,p.[ServerName] as Prod
FROM (SELECT DISTINCT [InstanceName] FROM SN) i
LEFT OUTER JOIN (SELECT [ServerName],[InstanceName] FROM SN WHERE [Environment] = 'DEV') d
	ON d.[InstanceName] = i.[InstanceName]
LEFT OUTER JOIN (SELECT [ServerName],[InstanceName] FROM SN WHERE [Environment] = 'QA') q
	ON q.[InstanceName] = i.[InstanceName]
LEFT OUTER JOIN (SELECT [ServerName],[InstanceName] FROM SN WHERE [Environment] IN ('SAVVIS','BI','OECPROD')) p
	ON p.[InstanceName] = i.[InstanceName]
GO

IF OBJECT_ID('[dbo].[ServerInventory_GetServerID]') IS NOT NULL
	DROP PROCEDURE [dbo].[ServerInventory_GetServerID]
GO
CREATE PROCEDURE [dbo].[ServerInventory_GetServerID] (
	@ServerName			VARCHAR(1000)
	,@ServerID			INT OUTPUT
)
AS

-- Find the ServerID
SELECT 
	@ServerID = [ServerID]
FROM 
	[dbo].[ServerInventory_ServerIDs] id
WHERE id.[ServerName] = @ServerName

IF @ServerID IS NULL
BEGIN
	INSERT INTO dbo.[ServerInventory_ServerIDs] (ServerName) 
	VALUES (@ServerName)
	
	SET @ServerID = SCOPE_IDENTITY()
END

GO

IF OBJECT_ID('[dbo].[ServerInventory_SQL_GetDatabaseID]') IS NOT NULL
	DROP PROCEDURE [dbo].[ServerInventory_SQL_GetDatabaseID]
GO
CREATE PROCEDURE [dbo].[ServerInventory_SQL_GetDatabaseID] (
	@DBName				SYSNAME
	,@DatabaseID		INT OUTPUT
)
AS

SELECT 
	@DatabaseID = [DatabaseID]
FROM 
	[dbo].[ServerInventory_SQL_DatabaseIDs] id
WHERE id.DBName = @DBName

IF @DatabaseID IS NULL
BEGIN
	INSERT INTO dbo.[ServerInventory_SQL_DatabaseIDs] (DBName) 
	VALUES (@DBName)
	
	SET @DatabaseID = SCOPE_IDENTITY()
END

GO

IF OBJECT_ID('[dbo].[ServerInventory_SQL_GetTableID]') IS NOT NULL
	DROP PROCEDURE [dbo].[ServerInventory_SQL_GetTableID]
GO
CREATE PROCEDURE [dbo].[ServerInventory_SQL_GetTableID] (
	@TableName			SYSNAME
	,@SchemaName		SYSNAME
	,@TableID			INT OUTPUT
)
AS

SELECT 
	@TableID = TableID
FROM 
	[dbo].[ServerInventory_SQL_TableIDs] id
WHERE id.[TableName] = @TableName
AND id.[SchemaName] = @SchemaName

IF @TableID IS NULL
BEGIN
	INSERT INTO [dbo].[ServerInventory_SQL_TableIDs] ([TableName], [SchemaName]) 
	VALUES (@TableName,@SchemaName)
	
	SET @TableID = SCOPE_IDENTITY()
END
GO

IF OBJECT_ID('[dbo].[ServerInventory_SQL_GetServerDBTableID]') IS NOT NULL
	DROP PROCEDURE [dbo].[ServerInventory_SQL_GetServerDBTableID]
GO
CREATE PROCEDURE [dbo].[ServerInventory_SQL_GetServerDBTableID] (
	@ServerName			VARCHAR(1000)
	,@DatabaseName		SYSNAME
	,@SchemaName		SYSNAME
	,@TableName			SYSNAME
	,@ServerDBTableID	INT OUTPUT
)
AS

DECLARE 
	@ServerID			INT
	,@DatabaseID		INT
	,@TableID			INT

-- Find the ServerID
EXEC [dbo].[ServerInventory_GetServerID] @ServerName, @ServerID OUTPUT

-- Find the DatabaseID
EXEC [dbo].[ServerInventory_SQL_GetDatabaseID] @DatabaseName, @DatabaseID OUTPUT

-- Find the TableID
EXEC [dbo].[ServerInventory_SQL_GetTableID] @TableName, @SchemaName, @TableID OUTPUT

-- Find the ServerDBTableID	
SELECT 
	@ServerDBTableID = ServerDBTableID
FROM
	dbo.[ServerInventory_SQL_ServerDBTableIDs] id
WHERE id.[ServerID]	= @ServerID
AND	id.[DatabaseID]	= @DatabaseID
AND id.[TableID]	= @TableID

-- Add the server/db/schema/table combo if necessary
IF @ServerDBTableID IS NULL
BEGIN
	INSERT INTO dbo.[ServerInventory_SQL_ServerDBTableIDs] ([ServerID], [DatabaseID], [TableID], [LastUpdated]) 
	VALUES (@ServerID, @DatabaseID, @TableID, GETDATE())
	
	SET @ServerDBTableID = SCOPE_IDENTITY()
END

GO